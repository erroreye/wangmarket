SAAS 云建站系统，可通过后台任意开通多个网站，每个网站使用自己的账号进行独立管理。让每个互联网公司都可私有化部署自己的 SAAS 云建站平台，一台 1 核 2G 服务器可建立几万个独立网站！  
使用方面，延续了帝国 CMS、织梦 CMS 的建站方式，有模版页面、模版变量、栏目绑定模版、内容管理等，用过帝国、织梦的，可快速使用！

## 核心理念
 
将成本（服务器、技术人员、网站制作、后续运维）降到最低，使（建站公司）利润最大化！

## 我们优势

1. **上云成本方面**，一台 1 核 2G 服务器可建立几万个独立网站。[甚至你如果只是做几个网站，用单机版无需买服务器！](https://e.gitee.com/leimingyun/doc/share/d88704307ba06edd/?sub_id=5624017)
2. **技术人员方面**，服务器运维及后端开发岗位（甚至前端工程师）也没了~~ 你从大学里招个计算机专业大学生，半天上手，流水线式做网站。
3. **安全稳定方面**，[百多次版本的更新迭代](http://www.wang.market/log.html)、使系统本身拥有强大的稳定及安全支撑，同时结合[网站分离插件](http://www.wang.market/35027.html)，将网站独立于服务器之外发布，数据持久性高达99.9999999999%、千万级并发、高可靠，彻底杜绝攻击、挂马等传统建站系统的安全隐患
4. **隐私担忧方面**，去中心化服务器控制，所有数据都在你自己服务器中。在某些时候，东西掌握在自己手里的才是最安全的。
5. **上手使用方面**，按照内置的引导步骤，从安装、选模板、生成网站，遵循下一步，下一步，完成，网站就出来了。同时完善的扩展及开发文档，帮您真正用得起来，觉得好用。
6. **定制扩展方面**，强大的[模板体系](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=5851390)让你网站想改那就改哪，比如服务客户常见的根据设计图制作网站，更是没有任何问题！另外后台功能方面也可以很方便的进行[二次开发及扩展](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=5808068)，如果你懂Java，你可以轻松对此系统进行任何改动。

## 两种版本

针对不同用户需求，我们CMS系统分为单机版（做单个网站）跟SAAS版（做多个网站）两种版本

* **单机版**：真正做到了去服务器。试想一下，你只是为了做一两个网站，就要买一台服务器，是不是太奢侈了！我们跟华为云深度结合，借助于最近云技术而出的一款完全属于你掌控的，无需服务器就能做出网站。此版本的详细说明及使用参见我们另一篇专门针对单机版的文档：  [https://e.gitee.com/leimingyun/doc/share/d88704307ba06edd/?sub_id=5621192](https://e.gitee.com/leimingyun/doc/share/d88704307ba06edd/?sub_id=5621192)
* **SAAS版**：安装到服务器，在线开通网站账号、每个网站完全独立管理。建站公司基本是采用此版本。本文档也是主要针对的SAAS版的。

## 功能方面

| | | | |
| --- | --- | --- | --- |
| [在线开通网站](http://wang.market/plugin/phoneCreateSite/reg.do?inviteid=50) | 网站独立管理 | [HTML模版体系](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=5851418)  | [百多套模板库](http://wang.market/template.do) |
| 自由绑定域名 |  自动分配二级域名 | 一键备份/还原 | [四十余种功能插件](http://www.wang.market/plugin.html) |
| [后台开放API](http://www.wang.market/35328.html) | [网站多语言](http://www.wang.market/35302.html) | [关键词替换](http://www.wang.market/35249.html) |  [数据库自动备份](http://www.wang.market/35041.html) |
| [支持OEM设置](http://www.wang.market/35040.html) | [网站跟后台分离](http://www.wang.market/35027.html) | [商城及支付能力](http://www.shop.zvo.cn) | [在线客服坐席](http://www.kefu.zvo.cn) |
| [完善的日志追踪](http://www.wang.market/31073.html) | [请求频率拦截](http://www.wang.market/31072.html) | [在线文件管理](http://www.wang.market/21019.html) | [sitemap自动生成](http://www.wang.market/19573.html) |
| [多个域名绑定](http://www.wang.market/19572.html) | [子账号管理网站](http://www.wang.market/13811.html) | [本地私有模版库](http://www.wang.market/10121.html) | [网站开放API](http://www.wang.market/10106.html) |
| [网站一键转移](http://www.wang.market/10104.html) | [网站侧边客服](http://www.wang.market/10103.html) | [万能表单反馈](http://www.wang.market/10068.html) | …… |

## 安装部署

根据上云不同，以及购买服务器方式不同、用途不同，我们提供了几种不同的安装方式，来应对不同的场景。  
其中最省事的当数[华为云官网提供的一键安装部署](https://www.huaweicloud.com/solution/implementations/build-a-CMS-based-on-open-source-wangmarket.html)的方式。（我们系统因优异表现被华为云官方收纳作为建站服务商的官方解决方案），您无需任何服务器等知识，只需进行填空，下一步、完成，即可自行完成整个安装。  
另外我们还针对其他云、自建机房、以及Windows用户提供了相应的一键部署方式，您可选择适合自身的方式进行安装部署。  
[点此查看各种部署方式及步骤说明](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=6000929)

## 入门使用

引导你怎么在线开通网站、去管理网站，并将网站绑定域名上线。详细步骤参见如下：  

1. [开通网站](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=5997142)
2. [网站管理后台操作](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=5997144)
3. [购买域名](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=5997145)
4. [解析域名，网站上线](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=5997262)

## 开发扩展

* **系统OEM**。可使用系统内置的 [OEM定制](http://www.wang.market/35040.html) 插件，在线更改建站系统名字了、帮助说明的链接地址了等。让这个建站系统看上去，更像是您自己公司自己开发的。
* **详细的模板开发文档**。 [环境及准备](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=5851390) | [模板开发入门](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=5851418) | [首页调取某个栏目最新6篇文章的示例](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=5851423)
* **完善的系统二次开发文档**。 [点此查阅](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=6010573)
* **丰富的扩展插件及详细插件制作方式**。 [当前插件列表](http://www.wang.market/plugin.html) | [插件开发文档](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=6010617)
* **网站等管理后台页面全自定义**。您可自由更高网站管理后台、代理后台、登录页面了等等页面的样式、显示内容、方式等。为自己量身定制自己的CMS系统。 [页面名字对应表](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=6010618)

## 目录结构
```
wangmarket                          项目
├─src                               项目源代码 ( Maven )
├─pom.xml                           项目源代码 pom ( Maven )
├─else                              其他的杂七杂八相关文件，一般用不到
│  ├─wangmarket.sql                 项目运行所需要的数据库文件( Mysql数据库，默认sqlite3)
└─README.md                         说明
```

## 配套产品

* **扒网站工具**，是一款桌面运行软件，看好哪个网站，输入要扒的 URL，自动扒下来做成 html 模版。并且所下载的 css、js、图片、html 文件会自动分好类保存到指定目录文件夹。所见网站，皆可为我所用。（目前已在开源爬虫领域全国第五）[官网&下载](http://www.templatespider.zvo.cn) | [使用说明](http://tag.wscso.com/8144.html)
* **网市场云商城系统**，是一款SAAS商城产品，提供商城购物及在线支付能力。跟网市场云建站可完美结合，用云建站的模板体系配合商城的API开放接口，来制作在线商城。 [云商城官网](http://www.shop.zvo.cn) | [源码开源](https://gitee.com/leimingyun/wangmarket_shop) | [私有化部署](https://gitee.com/leimingyun/dashboard/wikis/leimingyun/bed2ecca-8e2e-4b20-8099-10f09101b097/preview?doc_id=1532896&sort_id=4255147)
* **雷鸣云客服系统**，是一款SAAS客服产品，可赋予网市场云建站在线客服对话的能力。比如网站中有人通过客服发起咨询，客服坐席人员就能实时收到消息，跟用户进行互动会话。如果坐席人员不在线，还能通过微信实时提醒坐席人员。 [客服官网&功能体验](http://www.kefu.zvo.cn) | [前端对话的js框架开源](https://gitee.com/mail_osc/kefu.js) | [客服后端Java服务开源](https://gitee.com/leimingyun/yunkefu) | [私有化部署](https://gitee.com/leimingyun/dashboard/wikis/leimingyun/install/preview?sort_id=3940906&doc_id=1381432)
* **多语言翻译js库**，是一款全自动翻译组件，引入几行js代码就能使你的网站具备几十种语言的切换能力，而你本身网站的代码不需要做任何改动。 [开源仓库&使用方式](https://gitee.com/mail_osc/translate)

## 参与贡献

欢迎您将自己做的模板或插件分享贡献，您可以将做好的在gitee中进行分享出来，格式可参考：
[贡献模板](https://gitee.com/mail_osc/wangmarket_template_lmyglm1) | [贡献插件](https://gitee.com/leimingyun/wangmarket_plugin_learn_example)

# 优秀开源项目及社区推荐
[https://github.com/featbit/featbit](https://github.com/featbit/featbit) 100% 开源的 Feature Management 平台。将软件交付风险在面向最终用户前降至最低，随便瞎写也不会有bug，加速软件创新的历史进度。FeatBit 赋能全团队交付、管理软件的超能力。
[LinkWeChat](https://gitee.com/LinkWeChat/link-wechat) LinkWeChat 是基于企业微信的开源 SCRM 系统，是企业私域流量管理与营销的综合解决方案。  
[IoTSharp](https://gitee.com/IoTSharp) IoTSharp 是一个 基于.Net Core 开源的物联网基础平台， 支持 HTTP、MQTT 、CoAp 协议  
[流之云](https://gitee.com/ntdgg) 信息化、数字化服务提供商  

## 历史事件

* **2010年**，发布第一个建站尝试版，发布到中国站长站，下载量数千。[功能说明](http://www.xnx3.com/software/xxJspMql/20121102/8.html) | [系统下载](https://down.chinaz.com/soft/29191.htm)
* **2016年**，发布第一个成熟稳定的完全重构后的商业版本 wangmarket v1.0
* **2018年**，将源码开源，在当年 [开源中国2018年度数据报告中，最热门开源项目中排第八，前10中仅有我们一个是CMS建站方面](https://gitee.com/2018annual)
* **2019年**，在开源中国的CMS建站系统开源项目中，排全国第二。
* **2022年**，[华为云纳入官网](https://www.huaweicloud.com/solution/implementations/build-a-CMS-based-on-open-source-wangmarket.html)

## 版权方面

本系统十余项相关知识产权受国家法律保护。  
我们允许你完全可以将它来进行商用盈利，但请在 [网站管理后台保留我们的版权标识](https://e.gitee.com/leimingyun/doc/share/78815f06cd4554b7/?sub_id=6000930) 不要删掉。  
2022年是相对艰难的一年，我们建议大中型企业购买企业授权，提供更好的服务支持，小微、初创企业及工作室，用开源版本的就足够，创业不易，更应相互扶持，同时为国家贡献自己的一份力量，为本地就业做贡献！

## 联系我们

作者：管雷鸣  
微信：xnx3com  
QQ群：740332119  
微信公众号：wangmarket  
开源仓库：[https://gitee.com/mail_osc/wangmarket](https://gitee.com/mail_osc/wangmarket)

